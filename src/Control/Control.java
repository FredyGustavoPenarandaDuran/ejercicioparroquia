/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Modelo.Feligres;
import Vista.Vista;
import Modelo.Lista;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Jairo Sierra
 */
public class Control implements ActionListener {

    private Feligres f;
    private Vista v;
    private Lista l;

    public Control() {
    }

    public Control(Feligres f, Vista v, Lista l) {
        this.f = f;
        this.v = v;
        this.l = l;
        actionListener(this);
    }

    public void iniciar() {
        v.setTitle("SISTEMA PARROQUIA POO");
        v.setLocationRelativeTo(null);
        v.setVisible(true);
    }

    private void actionListener(ActionListener Control) {
        System.out.println("recibiendo boton Guardar");
        v.btnGuardar.addActionListener(Control);
        System.out.println("recibiendo boton Actualizar");
        v.btnActualizar.addActionListener(Control);
        System.out.println("recibiendo boton Buscar");
        v.btnBuscar.addActionListener(Control);
        System.out.println("recibiendo boton Eliminar");
        v.btnEliminar.addActionListener(Control);
        System.out.println("recibiendo boton Consultar");
        v.btnConsultar.addActionListener(Control);
        System.out.println("recibiendo boton Pagar");
        v.btnPagar.addActionListener(Control);
        System.out.println("recibiendo boton Totalizar");
        v.btnTotalizar.addActionListener(Control);
    }

    @Override
    public void actionPerformed(ActionEvent evento) {

        if (evento.getActionCommand().equalsIgnoreCase("Guardar")) {
            boolean estado = false;
            if (v.txtEstado.getText().equalsIgnoreCase("deudor")) {
                estado = true;
            } else if (v.txtEstado.getText().equalsIgnoreCase("pago")) {
                estado = false;
            } else {
                System.out.println("Estado no valido");
            }
            l.agregarFeligres(v.txtCedula.getText(), v.txtNombre.getText(), v.txtDireccion.getText(),
                    v.txtTelefono.getText(), Integer.parseInt(v.txtEstrato.getText()), estado, 0);

        } else if (evento.getActionCommand().equals("Buscar")) {

            System.out.println("boton Buscar");
            String cedula = JOptionPane.showInputDialog("Ingrese la cédula a buscar.");
            v.areaResultados.setText(String.valueOf(l.mostrarDatosFeligres(cedula)));

        } else if (evento.getActionCommand().equals("Eliminar")) {

            System.out.println("boton Eliminar");
            String cedula = JOptionPane.showInputDialog("Ingrese la cédula del feligres a eliminar.");
            boolean sePudo = l.eliminarFeligres(cedula);
            if (sePudo) {
                v.txtCedula.setText("");
                v.txtNombre.setText("");
                v.txtTelefono.setText("");
                v.txtEstrato.setText("");
                v.txtEstado.setText("");
                v.txtDireccion.setText("");
            }

        } else if (evento.getActionCommand().equals("Actualizar")) {

            JOptionPane.showMessageDialog(null, "Si cancela en alguno de los campos no se actualizara con exito los datos del feligres");
            String ccACT = JOptionPane.showInputDialog("Digite la Cedula del feligres que desea actualizar");
            String actCC = JOptionPane.showInputDialog("Digite la nueva Cedula del feligres");
            String actNombre = JOptionPane.showInputDialog("Digite el nuevo nombre del feligres");
            String actDireccion = JOptionPane.showInputDialog("Digite la nueva direccion del feligres");
            String actTelefono = JOptionPane.showInputDialog("Digite el nuevo telefono del feligres");
            int actEstrato = Integer.parseInt(JOptionPane.showInputDialog("Digite el nuevo estrato del feligres"));
            Feligres actF = new Feligres();
            actF.setNombre(actNombre);
            actF.setCedula(actCC);
            actF.setDireccion(actDireccion);
            actF.setTelefono(actTelefono);
            actF.setEstrato(actEstrato);

            v.areaResultados.setText(String.valueOf(l.actualizar(actF, ccACT)));

            JOptionPane.showMessageDialog(null, "Se ha actualizado con éxito");

        } else if (evento.getActionCommand().equals("Consultar")) {

            System.out.println("boton consultar");
            v.txtDValorDiezmo.setText(String.valueOf(l.ConsultarDiezmo(v.txtDCedula.getText())));

        } else if (evento.getActionCommand().equals("Pagar")) {

            System.out.println("boton pagar");
            l.pagar(v.txtDCedula.getText());
            v.areaResultados.setText(String.valueOf(l.mostrarDatosFeligres(v.txtDCedula.getText())));

        } else if (evento.getActionCommand().equals("Totalizar")) {
            System.out.println("boton totalizar");
            v.areaResultados.setText(String.valueOf(l.totalizarDiezmo()));

        }
    }

}
