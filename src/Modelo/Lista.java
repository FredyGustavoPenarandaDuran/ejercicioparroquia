/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Jairo Sierra
 */
public class Lista extends Feligres {
    // Feligres feligres = new Feligres();

    ArrayList<Feligres> feligreses = new ArrayList<>();

    public Lista() {
        super();
    }

    public Lista(ArrayList feligres) {
        this.feligreses = feligres;
    }

    //Feligres cedula, Feligres nombre, Feligres direccion, Feligres telefono, Feligres estrato, Feligres estado
    public void agregarFeligres(String cedula, String nombre, String direccion, String telefono, int estrato, boolean estado, int valorDiezmo) {

        Feligres f = new Feligres(cedula, nombre, direccion, telefono, estrato, estado, valorDiezmo);

        feligreses.add(f);
        System.out.println("Creación del feligres");
    }

    public String mostrarDatosFeligres(String ConsultaCC) {

        String datos = "NO EXISTE";

        for (int i = 0; i < feligreses.size(); i++) {
            if (feligreses.get(i).getCedula().equals(ConsultaCC)) {
                datos = String.valueOf(feligreses.get(i).toString());
            }
        }
        return datos;
    }

    public int ConsultarDiezmo(String ConsultaCC) {

        int Cdiezmo = 0;

        for (int i = 0; i < feligreses.size(); i++) {
            if (feligreses.get(i).getCedula().equals(ConsultaCC)) {
                if (feligreses.get(i).getEstado() == true) {
                    if (feligreses.get(i).getEstrato() == 1) {
                        System.out.println("Estrato 1");
                        Cdiezmo = 250000;
                        feligreses.get(i).setValorDiezmo(Cdiezmo);
                    } else if (feligreses.get(i).getEstrato() == 2 || feligreses.get(i).getEstrato() == 3) {
                        System.out.println("Estrato 2 o 3");
                        Cdiezmo = 500000;
                        feligreses.get(i).setValorDiezmo(Cdiezmo);
                    } else if (feligreses.get(i).getEstrato() > 3) {
                        System.out.println("Estrato más de 3");
                        Cdiezmo = 1000000;
                        feligreses.get(i).setValorDiezmo(Cdiezmo);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Consultar Diezmo:\n*El feligres no tiene deuda*");
                }
            }

        }

        return Cdiezmo;
    }

    public String actualizar(Feligres f, String cedulaAct) {
        String datos = "Prueba de actualizar datos fallida";
        for (int i = 0; i < feligreses.size(); i++) {
            if (feligreses.get(i).getCedula().equals(cedulaAct)) {
                feligreses.get(i).setCedula(f.getCedula());
                feligreses.get(i).setNombre(f.getNombre());
                feligreses.get(i).setDireccion(f.getDireccion());
                feligreses.get(i).setTelefono(f.getTelefono());
                feligreses.get(i).setEstrato(f.getEstrato());
                datos = String.valueOf(feligreses.get(i));
            } else {
                datos = "No existe";
            }

        }
        return datos;
    }

    public int totalizarDiezmo() {
        int totalDiezmo = 0;
        for (int i = 0; i < feligreses.size(); i++) {
            totalDiezmo = feligreses.get(i).getValorDiezmo();
        }
        return totalDiezmo;
    }

    public void pagar(String ConsultaCC) {

        for (int i = 0; i < feligreses.size(); i++) {
            if (feligreses.get(i).getCedula().equals(ConsultaCC)) {
                if (feligreses.get(i).getEstado() == true) {
                    if (feligreses.get(i).getEstrato() == 1) {
                        feligreses.get(i).setEstado(false);
                        JOptionPane.showMessageDialog(null, "Pagar Diezmo\n* DIEZMO PAGADO*");
                    } else if (feligreses.get(i).getEstrato() == 2 || feligreses.get(i).getEstrato() == 3) {
                        feligreses.get(i).setEstado(false);
                        JOptionPane.showMessageDialog(null, "Pagar Diezmo\n* DIEZMO PAGADO*");
                    } else if (feligreses.get(i).getEstrato() > 3) {
                        feligreses.get(i).setEstado(false);
                        JOptionPane.showMessageDialog(null, "Pagar Diezmo\n* DIEZMO PAGADO*");
                    }
                } else if (feligreses.get(i).getEstado() == false) {
                    JOptionPane.showMessageDialog(null, "Pagar Diezmo\n* YA VENIA DIEZMO PAGADO*");
                }
            } else if (!feligreses.get(i).getCedula().equals(ConsultaCC)) {
                JOptionPane.showMessageDialog(null, "No existe feligres");
            }
            feligreses.get(i).setValorDiezmo(0);
        }

    }

    public boolean eliminarFeligres(String ConsultaCC) {

        boolean sePudo=false;

        for (int i = 0; i < feligreses.size(); i++) {
            if (feligreses.get(i).getCedula().equals(ConsultaCC)) {
                
                int confirmacion = JOptionPane.showConfirmDialog(null, "¿Esta seguro que desea eliminar Feligres?",
                        "Eliminar Feligres", JOptionPane.YES_NO_OPTION);
                
                if(confirmacion==0){
                    
                    feligreses.remove(i);
                    System.out.println("*Se elimina*");
                    sePudo=true;
                    
                }
            }else{
                if (!feligreses.get(i).getCedula().equals(ConsultaCC)) {
                JOptionPane.showMessageDialog(null, "No existe feligres");
            }
            }
        }
        return sePudo;
    }
}
